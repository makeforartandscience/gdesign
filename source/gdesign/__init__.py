#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
gdesign's package provides classes and functions to manage 2D geometric objects
usage available in python 2.6+ and 3.x and for Inkscape extensions 

author : Thierry Dassé
version : 0.2.5
date : 07/19/2019
licence : cc-by-nc
"""

__version__ = '0.2.5'

from .gdesign import *
from .gear import *
