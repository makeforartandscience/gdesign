#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
gear package for gdesign
author : Thierry Dassé
version : 0.2.4
date : 04/11/2017
licence : cc-by-nc
"""

from .gdesign import *


class Gear(object):
    def __init__(self,z = 6, m = 1):
        self.m = m
        self.z = z

    def set(self,z):
        self.z = z

    def get_path(self):
        """
        return a standard gear path
        """

        p = Path()
        p.new_sub_path()

        alpha = radians(20) ## angle de pression
        k = 0.2             ## coefficient du congé de pied

        ## définitions des rayons
        r = self.m*self.z/2 ## rayon primitif
        r_b = r*cos(alpha) ## rayon de base
        if self.m > 1: ## rayon du pied de la denture (intérieur)
            r_i = r - 1.25*self.m
        else:
            r_i = r - 1.4*self.m
        r_e = r+self.m ## rayon de la tête de la denture (extérieur)

        r_c = r_i+k*self.m ## rayon de fin du congé

        ## développante de cercle
        t_a = sqrt(1/cos(alpha)**2-1) ## angle de départ de la développante
        t_b = sqrt((r_e/r_b)**2-1) ## angle d'arrivée de la développante
        e = ('{}*(cos(t)+t*sin(t))'.format(r_b),'{}*(sin(t)-t*cos(t))'.format(r_b))

        p_d = Parametric(e,(t_a,t_b))

        axis = Line(Point(0,0),Point(1,0))

        i_a = p_d.get(t_a).angle() ## angle de départ
        b_d = p_d.to_bcurve().rotate(Point(0,0),-pi/(2*self.z)-i_a) ## courbe développante ascendante
        b_d2 = b_d.copy().reflect(axis) ## courbe développante descendante
        b_d2.reverse()
        l_e = Polyline() ## ligne extérieure
        l_e.add_node(b_d.end())
        l_e.add_node(b_d2.start())
        l_b = Polyline() ## ligne de base de la dent ascendante
        p_b = b_d.start() ## point de départ de la courbe développante ascendante
        p_c = Point()
        p_c.set_polar(r_c,p_b.angle())
        l_b.add_node(p_c)
        l_b.add_node(p_b)
        l_b2 = Polyline() ## ligne de base de la dent descendante
        p_b2 = b_d2.end() ## point d'arrivée de la courbe développante descendante
        p_c2 = Point()
        p_c2.set_polar(r_c,p_b2.angle())
        l_b2.add_node(p_b2)
        l_b2.add_node(p_c2)
        
##        a_i = Parametric() ## arc du pied de la roue dentée
##        a_i.set(('{}*cos(t)'.format(r_i),'{}*sin(t)'.format(r_i)),(pi/(2*self.z)+k*self.m/r_i,1.5*pi/self.z-k*self.m/r_i))
##        a = a_i.to_bcurve()

        a = Arc(Point(0,0),r_i,r_i,0,pi/(2*self.z)+k*self.m/r_i,1.5*pi/self.z-k*self.m/r_i)
        
        p.add(l_b)
        p.add(b_d)
        p.add(l_e)
        p.add(b_d2)
        p.add(l_b2)
        p.add(a)

        for i in range(1,self.z):
            p.add(l_b.copy().rotate(Point(0,0),2*i*pi/self.z))
            p.add(b_d.copy().rotate(Point(0,0),2*i*pi/self.z))
            p.add(l_e.copy().rotate(Point(0,0),2*i*pi/self.z))
            p.add(b_d2.copy().rotate(Point(0,0),2*i*pi/self.z))
            p.add(l_b2.copy().rotate(Point(0,0),2*i*pi/self.z))
            p.add(a.copy().rotate(Point(0,0),2*i*pi/self.z))
            
        return p.close()
