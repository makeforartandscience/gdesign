#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

import gdesign

setup(
name='gdesign',
version=gdesign.__version__,
description=gdesign.__doc__,
packages=find_packages(),
long_description=open('README.md').read(),
include_package_data=True,
)
